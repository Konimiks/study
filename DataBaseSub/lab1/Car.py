class Car(object):
    def __init__(self, id, engineVolume, color):
        self.id = id
        self.engineVolume = engineVolume
        self.color = color

    def __str__(self):
        return "id = %s, engineVolume = %s, color = %s" % (self.id, self.engineVolume, self.color)

    def editCarEV(self, EV):
        self.engineVolume = EV

    def editCarColor(self, COLOR):
        self.color = COLOR
