class Firm(object):
        def __init__(self, name, id):
            self.name = name
            self.id = id
            self.carsDict = dict()

        def __str__(self):
            return "id = %s, name = %s" % (self.id, self.name)

        def printFirmCars(self):
            if(self.carsDict):
                for car_k, car_v in self.carsDict.items():
                    print(car_v)
            else:
                print("Firm has NO Cars")

        def editFirmName(self, newName):
            self.name = newName

        def existsCar(self, id):
            if id in self.carsDict:
                return True
            return False

        def addCartoFirm(self, newCar):
            if (self.existsCar(newCar.id)):
                print("Car already exists")
            else:
                self.carsDict[newCar.id] = newCar
                print("Car was added")

        def listOfCarsWithCertainEV(self, EV):
            list = []
            for car_k, car_v in self.carsDict.items():
                if(car_v.engineVolume == (EV)):
                    list.append(car_v)
            return list