from Firm import Firm
from Car import Car


class Firms(object):
    def __init__(self):
        self.firmsDict = dict()

    def printFirms(self):
        resultFirmsArray = []
        if (self.firmsDict):
            for firm_k, firm_v in self.firmsDict.items():
                resultFirmsArray.append(firm_v)
            return resultFirmsArray
        else:
            #print("NO Firm exists yet")
            return False

    def printCars(self,id_f):
        if (self.exists(id_f)):
            if (self.firmsDict[id_f].carsDict):
                for car_k, car_v in self.firmsDict[id_f].carsDict.items():
                    print(car_v)
            else:
                print("Firm has NO CARS")
        else:
           print("NO Firm with id " + id_f)

    def addFirm(self, firmId, firmName):
        if self.exists(id):
            print("Firm with ID " + firmId + " already exists!")
        else:
            firm = Firm(firmName, firmId)
            self.firmsDict[firmId] = firm
            print("Firm added!")

    def addCar(self, id_f ,id_c, volume, color):
        car = Car(id_c, volume, color)
        self.firmsDict[id_f].addCartoFirm(car)

    def exists(self, id):
        if id in self.firmsDict:
            return True
        return False

    def deleteFirm(self, id):
        if self.exists(id):
            del self.firmsDict[id]
            print("FIRM was deleted")
        else:
            print("NO FIRM with ID " + id)

    def deleteCar(self, id):
        for firm_k, firm_v in self.firmsDict.items():
            if id in firm_v.carsDict:
                del firm_v.carsDict[id]
                print("CAR was deleted")
                return True
        print("NO CAR with ID " + id)
        return False

    def editFirm(self, firmId, newValue):
        if self.exists(firmId):
            self.firmsDict[firmId].editFirmName(newValue)
            print("Editing done! Firm with id " + firmId + " has new name: " + newValue)
        else:
            print("NO Firm with id " + firmId)

    def editCarEV(self, id_c, EV):
        for firm_k, firm_v in self.firmsDict.items():
            for car_k, car_v in firm_v.carsDict.items():
                if car_k == id_c:
                    car_v.editCarEV(EV)
                    print("Editing done! Car with ID "+ id_c + " has new ENGINE VOLUME: " + EV)
                    return True
        print("NO CAR with ID " + id_c)
        return False

    def editCarColor(self, id_c, Col):
        for firm_k, firm_v in self.firmsDict.items():
            for car_k, car_v in firm_v.carsDict.items():
                if car_k == id_c:
                    car_v.editCarColor(Col)
                    print("Editing done! Car with ID " + id_c + " has new COLOR: " + Col)
                    return True
        print("NO CAR with ID " + id_c)
        return False

    def showFirm(self, id):
        if id in self.firmsDict:
            print(self.firmsDict[id])
        else:
            print("NO FIRM with ID " + id)

    def showCar(self, id):
        check = 0
        for firm_k, firm_v in self.firmsDict.items():
            if id in firm_v.carsDict:
                check = 1
                print(firm_v.name)
                print(firm_v.carsDict[id])
        if (check == 0):
            print("NO CAR with ID " + id)

    def existCar(self,id_c):
        for firm_k, firm_v in self.firmsDict.items():
            if id_c in firm_v.carsDict:
                return True
        return False

    def filterByEV(self,EV):
        resultList = []
        for firm_k, firm_v in self.firmsDict.items():
            resultList.extend(firm_v.listOfCarsWithCertainEV(EV))
        return resultList

