import pickle

class Writer(object):
    def __init__(self, fileName):
        self.fileName = fileName

    def save(self, data):
        with open(self.fileName, 'wb') as f:
            pickle.dump(data, f)

    def giveBack(self):
        dict = {}
        with open('data.pickle', 'rb') as f:
            dict = pickle.load(f)
        for item_k, item_v in dict.items():
            print(item_v)
            for it_k, it_v in item_v.carsDict.items():
                print(it_v)
        return dict;


