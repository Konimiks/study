from constants import Constants
from Firm import Firm
from Car import Car
import sys
from Writer import Writer


class Controller(object):
    def __init__(self, model, view):
        self.model = model
        self.view = view
        self.constants = Constants()

    def runUser(self):
        check = True
        writer = Writer("data.pickle")
        self.model.firms.firmsDict = writer.giveBack()
        self.view.printMenu()
        while check:
            choice = self.inputValue()
            check = self.switchHandler(choice)
        self.view.printMessage("Bye!")
        writer.save(self.model.firms.firmsDict)
        sys.exit(0)

    def switchHandler(self, choice):
        if choice == Constants.SHOW_FIRMS:
            self.view.printFirmsHandler(self.model.firms.printFirms())
            return True
        if choice == Constants.SHOW_FIRM_CARS:
            self.view.printMessage(self.constants.ID_F)
            choice = input()
            self.model.firms.printCars(choice)
            return True
        if choice == Constants.SHOW_FIRM:
            self.view.printMessage(self.constants.ID_F)
            choice = self.inputValue()
            self.model.firms.showFirm(choice)
            return True
        if choice == Constants.SHOW_CAR:
            self.view.printMessage(self.constants.ID_C)
            choice = input()
            self.model.firms.showCar(choice)
            return True
        if choice == Constants.ADD_FIRM:
            self.view.printMessage(self.constants.ID_F)
            id_f = input()
            if self.model.firms.exists(id_f):
                self.view.printMessage("Firm with ID " + id_f + " already exist!")
            else:
                self.view.printMessage(self.constants.NAME)
                name = input()
                self.model.firms.addFirm(id_f, name)
            return True
        if choice == Constants.ADD_CAR:
            self.view.printMessage(self.constants.ID_F)
            id_f = input()
            if self.model.firms.exists(id_f):
                self.view.printMessage(self.constants.ID_C)
                id_c = input()
                if self.model.firms.existCar(id_c):
                    self.view.printMessage("Car with ID " + id_c + " already exist!")
                    return True
                else:
                    self.view.printMessage(self.constants.ENGINE_VOLUME)
                    volume = int(input())
                    self.view.printMessage(self.constants.COLOR)
                    color = input()
                    self.model.firms.addCar(id_f, id_c, volume, color)
            else:
                self.view.printMessage("NO FIRM with ID " + id_f)
            return True
        if choice == Constants.DELETE_FIRM:
            self.view.printMessage(self.constants.ID_F)
            id_f = input()
            self.view.printMessage("All Cars of this Firm will be deleted too. Are you sure? Y/N")
            choice = input()
            if choice == "Y":
                self.model.firms.deleteFirm(id_f)
                return True
            else:
                return True
        if choice == Constants.DELETE_CAR:
            self.view.printMessage(self.constants.ID_C)
            id_c = input()
            self.model.firms.deleteCar(id_c)
            return True
        if choice == Constants.EDIT_FIRM:
            while choice != "q":
                self.view.printMenuEF()
                choice = self.inputValue()
                self.switchHandlerEF(choice)
            return True
        if choice == Constants.EDIT_CAR:
            while choice != "q":
                self.view.printMenuEC()
                choice = self.inputValue()
                self.switchHandlerEC(choice)
            return True
        if choice == Constants.EXIT:
            return False
        if choice == Constants.SHOW_FILTERED_LIST:
            EV = int(input("Enter Engine Volume, please "), 10)
            self.view.printFilteredList(self.model.firms.filterByEV(EV))
            return True

    def inputValue(self):
        print("Enter command: ")
        str = input()
        return str

    def switchHandlerEF(self, choice):
        if choice == "NAME":
            self.view.printMessage(self.constants.ID_F)
            id_f = self.inputValue()
            if self.model.firms.exists(id_f):
                self.view.printMessage(self.constants.NAME)
                name = self.inputValue()
                self.model.firms.editFirm(id_f, name)
            else:
                self.view.printMessage("NO FIRM with ID " + id_f)

    def switchHandlerEC(self, choice):
        if choice == "ENGINE_VOLUME":
            self.view.printMessage(self.constants.ID_C)
            id_c = self.inputValue()
            if():
                self.view.printMessage(self.constants.ENGINE_VOLUME)
                ENGINE_VOLUME = self.inputValue()
                self.model.firms.editCarEV(id_c, ENGINE_VOLUME)
            else:
                self.view.printMessage("NO CAR with ID " + id_c)
        if choice == "COLOR":
            self.view.printMessage(self.constants.ID_C)
            id_c = self.inputValue()
            if ():
                self.view.printMessage(self.constants.COLOR)
                COLOR = self.inputValue()
                self.model.firms.editCarColor(id_c, COLOR)
            else:
                self.view.printMessage("NO CAR with ID " + id_c)


