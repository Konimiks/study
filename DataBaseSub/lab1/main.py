from model import Model
from view import View
from controller import Controller

view = View()
model = Model()
controller = Controller(model, view)
controller.runUser()
