from constants import Constants


class View(object):
    def printMenu(self):
        print(Constants.SHOW_FIRMS)
        print(Constants.SHOW_FIRM_CARS)
        print(Constants.SHOW_FIRM)
        print(Constants.SHOW_CAR)
        print(Constants.ADD_FIRM)
        print(Constants.ADD_CAR)
        print(Constants.DELETE_FIRM)
        print(Constants.DELETE_CAR)
        print(Constants.EDIT_FIRM)
        print(Constants.EDIT_CAR)
        print(Constants.SHOW_FILTERED_LIST)
        print(Constants.EXIT)

    def printMenuEF(self):
        print(Constants.NAME + " - for change FIRM name")
        print("q - for stop change FIRM name")

    def printMenuEC(self):
        print(Constants.ENGINE_VOLUME + " - for change CAR ENGINE VOLUME")
        print(Constants.COLOR + " - for change CAR COLOR")
        print("q - for stop change FIRMs name")

    def printMessage(self, message):
        print(message)

    def printFilteredList(self, list):
        for item in list:
            self.printMessage(item)

    def printFirmsHandler(self, result):
        if (result == False):
            self.printMessage("NO Firm exists yet")
        else:
            self.printMessage("Firms:")
            for item in result:
                print(item)

